import sys
import time
import logging
import urllib3
import argparse

try:
    parser = argparse.ArgumentParser(description='Store locally accessible file to CTERA edge filer.  It should be called once per file to upload.')
    parser.add_argument('-d', '--destination', action='store', dest='destination', type=str, required=True, help='Destination share/path (Fromat: share/folder1/folder2/folderN')
    parser.add_argument('-i', '--ip', action='store', dest='ip', type=str, required=True, help='IP address or DNS name of target device')
    parser.add_argument('-u', '--user', action='store', dest='user', type=str, required=True, help='User with access to the CTERA Edge Filer')
    parser.add_argument('-p', '--password', action='store', dest='password', type=str, help='Password for the CTERA user')
    parser.add_argument('-s', '--source', action='store', dest='source', type=str, required=True, help='Full path to the file that needs to be transferred (Format: C:\\Users\\First Last\\Desktop\\sample.txt)')
    parser.add_argument('-l', '--log-file', action='store', dest='log_file', type=str, help='Full path to the log file(Format: C:\\Users\\First Last\\Desktop\\logfile.log)')
    parser.add_argument('--debug', action='store_true', dest='is_debug', default=False, help='Enables debug output of arguments')
    
    arguments = parser.parse_args()
    if (arguments.password is None):
        arguments.password = 'This_is_my_secure_password'
    
    if (arguments.log_file is None):
        arguments.log_file = 'file_to_edge.log'
        
    if (arguments.is_debug):
        print('destination =', arguments.destination)
        print('ip =', arguments.ip)
        print('user =', arguments.user)
        print('source =', arguments.source)
        print('log file =', arguments.log_file)
        print('is_debug =', arguments.is_debug)
        
except KeyboardInterrupt:
    logging.getLogger().fatal('Cancelled by user.')

import os 
os.environ['CTERASDK_LOG_FILE'] = arguments.log_file

from cterasdk import config, CTERAException, Gateway, gateway_enum, gateway_types, Object
from cterasdk.lib.filesystem import FileSystem
from cterasdk.common.utils import convert_size, DataUnit
from cterasdk.edge.files.mkdir import ItemExists

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

if __name__ == '__main__':

    config.http['ssl'] = 'Trust'  # ignore certificate errors connecting to CTERA Edge Filer
    config.Logging.get().setLevel(logging.INFO)  # enable debug
    
    try:             
        filer = Gateway(arguments.ip, 443, True)   #Instantiate filer object
        filer.login(arguments.user, arguments.password)    #Login to filer
        filer.files.upload(arguments.source, arguments.destination)  #Upload file
        #filer.files.upload('C:\\Users\\First Last\\Desktop\\sample.txt', 'cloud-acls/test')
        filer.logout()     #Logout
    
    except CTERAException as error:
        logging.getLogger().fatal(error.message)
    except KeyboardInterrupt:
        logging.getLogger().fatal('Cancelled by user.')
