from cterasdk import *
import urllib3

if __name__ == "__main__":
    config.http['ssl'] = 'Trust' 
    urllib3.disable_warnings()
    
    try:
        admin = GlobalAdmin('192.168.1.38')#portal
        admin.login('admin', 'password') #edit this with portal admin
        filers = admin.devices.filers()
        print("\n")
        for filer in filers:
            try:
                device = Gateway(filer.name, 443, True)
                device.login('admin','password')  #edit this with device admin
                status = device.get('/proc/cloudsync')
                settings = device.get('/config/device')
                    
                print(settings.hostname)
                print("Files in upload: " + str(status.serviceStatus.uploadingFiles))
                print("Files in download: " + str(status.serviceStatus.downloadingFiles))
                print("\n")
            except CTERAException as error:
                print(error)
    except CTERAException as error:
        print(error)
